# script-version: 2.0
# Catalyst state generated using paraview version 5.10.0

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [789, 539]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.00014495849609375, 3.5762786865234375e-05, 5.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-7.759826206632352, -17.448749360728215, 23.473707370010725]
renderView1.CameraFocalPoint = [2.0121337360706537, 4.524119564616456, 0.21017654347910097]
renderView1.CameraViewUp = [-0.3113350384346504, 0.75267110724, 0.5801350689012061]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 8.659769789105821
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(789, 539)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Unstructured Grid Reader'
smoothinglength044run01vtuseries = XMLUnstructuredGridReader(registrationName='smoothinglength0.44-run01.vtu.series', FileName=['/gpfs/alpine/world-shared/stf007/msandov1/scivis_datasets/scivis_2016_particular_ensembles/smoothinglength0.44-run01.vtu.series'])
smoothinglength044run01vtuseries.PointArrayStatus = ['concentration', 'velocity']
smoothinglength044run01vtuseries.TimeArray = 'None'

# create a new 'Convert To Point Cloud'
convertToPointCloud1 = ConvertToPointCloud(registrationName='ConvertToPointCloud1', Input=smoothinglength044run01vtuseries)
convertToPointCloud1.CellGenerationMode = 'Vertex cells'

# create a new 'Point Volume Interpolator'
pointVolumeInterpolator1 = PointVolumeInterpolator(registrationName='PointVolumeInterpolator1', Input=convertToPointCloud1,
    Source='Bounded Volume')
pointVolumeInterpolator1.Kernel = 'GaussianKernel'
pointVolumeInterpolator1.Locator = 'Static Point Locator'

# init the 'GaussianKernel' selected for 'Kernel'
pointVolumeInterpolator1.Kernel.Radius = 0.1

# init the 'Bounded Volume' selected for 'Source'
pointVolumeInterpolator1.Source.Origin = [-4.998645782470703, -4.9995598793029785, 0.0]
pointVolumeInterpolator1.Source.Scale = [9.998610973358154, 9.998910427093506, 10.0]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pointVolumeInterpolator1
pointVolumeInterpolator1Display = Show(pointVolumeInterpolator1, renderView1, 'UniformGridRepresentation')

# get color transfer function/color map for 'concentration'
concentrationLUT = GetColorTransferFunction('concentration')
concentrationLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 178.59500122070312, 0.865003, 0.865003, 0.865003, 357.19000244140625, 0.705882, 0.0156863, 0.14902]
concentrationLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'concentration'
concentrationPWF = GetOpacityTransferFunction('concentration')
concentrationPWF.Points = [0.0, 0.0, 0.5, 0.0, 357.19000244140625, 1.0, 0.5, 0.0]
concentrationPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pointVolumeInterpolator1Display.Representation = 'Volume'
pointVolumeInterpolator1Display.ColorArrayName = ['POINTS', 'concentration']
pointVolumeInterpolator1Display.LookupTable = concentrationLUT
pointVolumeInterpolator1Display.SelectTCoordArray = 'None'
pointVolumeInterpolator1Display.SelectNormalArray = 'None'
pointVolumeInterpolator1Display.SelectTangentArray = 'None'
pointVolumeInterpolator1Display.OSPRayScaleArray = 'concentration'
pointVolumeInterpolator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
pointVolumeInterpolator1Display.SelectOrientationVectors = 'None'
pointVolumeInterpolator1Display.SelectScaleArray = 'None'
pointVolumeInterpolator1Display.GlyphType = 'Arrow'
pointVolumeInterpolator1Display.GlyphTableIndexArray = 'None'
pointVolumeInterpolator1Display.GaussianRadius = 0.05
pointVolumeInterpolator1Display.SetScaleArray = ['POINTS', 'concentration']
pointVolumeInterpolator1Display.ScaleTransferFunction = 'PiecewiseFunction'
pointVolumeInterpolator1Display.OpacityArray = ['POINTS', 'concentration']
pointVolumeInterpolator1Display.OpacityTransferFunction = 'PiecewiseFunction'
pointVolumeInterpolator1Display.DataAxesGrid = 'GridAxesRepresentation'
pointVolumeInterpolator1Display.PolarAxes = 'PolarAxesRepresentation'
pointVolumeInterpolator1Display.ScalarOpacityUnitDistance = 0.173190770864269
pointVolumeInterpolator1Display.ScalarOpacityFunction = concentrationPWF
pointVolumeInterpolator1Display.OpacityArrayName = ['POINTS', 'concentration']
pointVolumeInterpolator1Display.SliceFunction = 'Plane'
pointVolumeInterpolator1Display.Slice = 50

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pointVolumeInterpolator1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 357.19000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pointVolumeInterpolator1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 357.19000244140625, 1.0, 0.5, 0.0]

# init the 'Plane' selected for 'SliceFunction'
pointVolumeInterpolator1Display.SliceFunction.Origin = [0.0006597042083740234, -0.00010466575622558594, 5.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for concentrationLUT in view renderView1
concentrationLUTColorBar = GetScalarBar(concentrationLUT, renderView1)
concentrationLUTColorBar.Title = 'concentration'
concentrationLUTColorBar.ComponentTitle = ''

# set color bar visibility
concentrationLUTColorBar.Visibility = 1

# show color legend
pointVolumeInterpolator1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup extractors
# ----------------------------------------------------------------

# create extractor
pNG1 = CreateExtractor('PNG', renderView1, registrationName='PNG1')
# trace defaults for the extractor.
pNG1.Trigger = 'TimeStep'

# init the 'PNG' selected for 'Writer'
pNG1.Writer.FileName = 'RenderView1_{timestep:06d}{camera}.png'
pNG1.Writer.ImageResolution = [1578, 1078]
pNG1.Writer.Format = 'PNG'

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pNG1)
# ----------------------------------------------------------------

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.ExtractsOutputDirectory = 'tutorial'
options.GlobalTrigger = 'TimeStep'
options.CatalystLiveTrigger = 'TimeStep'

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    from paraview.simple import SaveExtractsUsingCatalystOptions
    # Code for non in-situ environments; if executing in post-processing
    # i.e. non-Catalyst mode, let's generate extracts using Catalyst options
    SaveExtractsUsingCatalystOptions(options)
