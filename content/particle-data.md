---
title: "Particle Data"
weight: 30
---

<img src="thumbnail.png" style="float: right;" />

# Particle Data

In this section of the tutorial we will explore data from a particle
simulation of fluid dynamics.

<div style="clear: both;"></div>

The data for this tutorial is currently available on OLCF at
`/lustre/orion/world-shared/stf007/msandov1/scivis_datasets/scivis_2016_particular_ensembles`.
The data originally comes from the SciVis 2016 contest:
https://www.uni-kl.de/sciviscontest/. The simulations were set up as a
cylinder filled with pure water. At the top of the cylinder is an unlimited
supply of salt. The salt dissolves on the top, and then because the
solution is heavier than pure water it sinks. The areas of high
concentration and that flow down are called viscous fingers.

## Connect to Andes

One node is sufficient. That data are not partitioned for more nodes.

## Open one of the File Sets

1. Click on `Open` file {{< icon Open >}}
2. Navigate to `/lustre/orion/world-shared/stf007/msandov1/scivis_datasets/scivis_2016_particular_ensembles`
3. Open `smoothinglength0.44-run01.vtu.series`
4. `Apply`

## Viewing the Particles as a Point Cloud

When you first load the data into ParaView, you might be surprised to see
nothing in the view. This is because this is a "meshless" dataset. That is,
there is no mesh or topology associated with this data. The data is just a
collection of points. To visualize this data as a point cloud, we first
must build a mesh of disconnected "vertex" cells.

1. Add the `Convert To Point Cloud` filter
   * You can find this in the menu under `Filters` -> `Alphabetical` ->
     `Convert To Point Cloud`
   * Alternately, you can bring up the quick search dialog (ctrl-space on
     Windows/Linux or option-space on Mac) and type `Point Cloud`
2. Change `Cell Generation Mode` to `Vertex cells`
3. `Apply`
4. Reset the camera {{< icon ResetCamera >}}
5. Color by `concentration`

You can now play through the time steps to see an animation of the flow.

![](point-cloud.png)

## Seeing Internal Points

One of the problems we are seeing with the data is that the particles are
so dense that we cannot see any particles except those that are on the
surface. We want to see what is happening inside the volume. What we really
care about is the flow of the highly concentrated salt mixture, so let's
focus on that by making pure water transparent.

1. Open the color map editor {{< icon EditColor >}}
2. Turn on the `Enable Opacity Mapping For Surfaces` option
3. Play with the opacity transfer function to get the desired effect
4. (Optional) Add better shading to the particles by changing the
   representation to `Point Gaussian`

Try playing through the timesteps again, and you will better see the flow.

![](internal-points.png)

## Slice Plane (attempt 1)

A common problem encountered when visualizing particle data is that many
filters do not work as expected because they need to query properties at
arbitrary points in space, and particle data does not have this
information. We will demonstrate this problem with the `Slice` filter.

1. Add the `Slice` filter {{< icon Slice >}}
2. `Apply`
3. Turn off the visibility {{< icon Eyeball >}} of `Convert To Point Cloud`
   (or any pipeline object other than `Slice`)

![](slice-fail.png)

One would expect that the `Slice` filter would extract the particles at
this plane in space. But instead, the slice is completely empty. Why is
that?

The problem is that our data does not define a field across a continuous
volume in space. Instead, information is stored only at infinitesimal
points in space. Everything else is just empty volume. Thus, the `Slice`
will not show a particle unless the particle just happens to lie exactly in
the plane. For particles in general position, it is unlikely for any
particle to hit this exact criterion.

To make filters like `Slice` work, we need to interpolate data in the space
between the particles. Let's delete the `Slice` filter and try again.

## Slice Plane (attempt 2)

Before applying the slice operation, we first have to interpolate the
points to a volumetric mesh.

1. Add the `Point Volume Interpolator`
2. Change the `Kernel` to `GaussianKernel`
3. Change `Radius` to `0.1`
4. Turn off `Show Box`
5. `Apply`
6. Add the `Slice` filter {{< icon Slice >}}
7. `Apply`

![](slice-success.png)

Success. We can now see concentration of the liquid in the slice. You might
want to open the color map editor and turn off the ` Enable Opacity Mapping
For Surfaces`.

## Volume Rendering

Interpolating the particles to a mesh opens up many visualization
opportunities. For example, we can now volume render the data.

1. Delete the `Slice` filter
2. Change the representation of the `Point Volume Interpolator` to `Volume`

![](volume-render.png)

As you can see, the `Point Volume Interpolator` is a very convenient filter
to convert particle data to a mesh. However, the filter usually needs to be
tweaked to get the particles to influence the proper range of volume around
them. The default kernel for interpolation, `VoronoiKernel` fills all space
using the data from the closest particle, which is fine except that it
fills up regions of space that should be empty. The `GaussianKernel`, which
we used in this example, limits the influence of each particle to nearby
space, but you must adjust the radius of influence to something that makes
sense. (For the data we are using in this example, a radius of `0.1` works
well.) Also, you will likely need to adjust the resolution of the created
mesh to match the density of particles. In this example, we used the
default resolution of {{< katex >}}100^3{{< /katex >}}, but that might need
to be adjusted up or down to capture the details of the data.

---

Next: [Batch Processing]({{< relref batch-processing.md >}})
