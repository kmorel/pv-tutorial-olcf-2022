---
title: "Batch Processing"
weight: 40
---

# Batch Processing

So far, we have discussed how to do interactive visualization on Andes.
This is helpful for exploration. But sometimes you have a visualization
process that you know will take a while or you need to apply repeatedly.
For these situations, you can set up a batch script and execute
visualization automatically.

ParaView uses Python as its scripting language. The ParaView GUI contains
several features that make it easy to export scripts with little or no
programming.

## Establish Visualization

To export a script from ParaView, you first need to set up a visualization
to do. If you are not there right now, do one of the other exercises in
this tutorial that connects to Andes, loads data, and shows a visualization
of that data such as the [particle visualization].

## Set up Exports

The batch script we are going to generate is going to replicate the
visualization we created. But during the batch processing, no one will be
there to see what is being produced. Instead, we need the batch processing
to write results to files. To specify what ParaView will write out, we use
something called _extractor_.

An extractor is an item that is placed in ParaView's pipeline that is a
placeholder for data we want to capture. The extractor will not immediately
do anything at the time it is applied, but it will be used later during
batch processing to determine what files will be written.

Extractors are available under the `Extractors` menu. There are two general
types of extractors: data and image. A data extractor is attached to an
object in the pipeline, much like a filter is. During batch processing, the
extractor will save out that data in a specified format. An image extractor
will grab the render view and write out images.

For the purposes of this exercise, we will set up an image extractor.

1. Add a png image extractor: `Extractors` -> `Image` -> `PNG`
2. Look for the `Image Resolution` and increase the resolution of the
   images generated

![](exports.png)

## Save some Extracts

We will get to the actual batch processing in a moment, but for now let's
use the ParaView GUI to extract data. This is useful if you want to set up
a visualization in the GUI and use the extraction to write a time-series of
data. It is also useful to verify what the extraction will do.

1. Click on `Save Extracts` {{< icon ExportScene >}} (in the `File` menu or
   on the toolbar)
2. Choose a temporary directory on your computer for the output
3. Click `OK`

A dialog box will show the progress of the extraction. Look at your save
directory to verify that image files are being written.

This might take a while, so you can hit `Abort` after a bit.

## Create a Batch Script

We are now ready to create a Python script that can be used to do batch
visualization. ParaView has the ability to save its current state as a
Python script.

1. Click on `Save Catalyst State` {{< icon Catalyst >}} (in the `File` menu or on the toolbar)
2. Select a destination for the batch script
3. Choose a directory to write data to, which will be relative to where the
   scripting is run
4. Click `OK`

Take a look at the [resulting script](render-particles.py). In it you
should recognize some of the parameters you set up in the GUI, such as the
filename being loaded. This gives you an opportunity to make changes to
what the script will do (for example, to change the filename), but for this
example we will run the script as is. We won't go into details on ParaView
script writing here. The [ParaView Guide] contains lots of information
about the ParaView script bindings.

## Copy the Batch file to Andes

When you run the ParaView batch scripting, it will happen exclusively on
Andes. Copy the script you generated to a shared directory that the Andes
compute nodes can access. You should be able to use the `scp` command for
that. (Windows users may need to download a version of `scp` or some other
secure network copy program.)

## Create Submission Script

Andes uses Slurm to schedule jobs. Details for submitting jobs on Andes can
be found in the [Andes User Guide] and is beyond the scope of the tutorial.
Suffice it to say you first need to create a submission batch shell script,
which will look [something like this](render-particles.slurm):

```bash
#!/bin/bash
#SBATCH -A csc143
#SBATCH -N 1
#SBATCH -t 0:15:00
#SBATCH -p gpu

module purge
module load paraview/5.10.0-egl

cd $SLURM_SUBMIT_DIR
date
srun -n 1 pvbatch render-particles.py
```

The first thing the script does is load the `paraview` module on the
system. This gives the script access to the `pvbatch` executable, which
will run ParaView scripts in an MPI job. Note that the `egl` version of the
ParaView module is loaded. EGL is a library that gives ParaView access to
the NVIDIA graphics cards on the GPU nodes of Andes. For this to work, the
batch script must be run on the `gpu` queue (using the `-q` argument). If
you are not using the GPU nodes of Andes, you should load the `osmesa`
version of the ParaView module, which uses CPU rendering.

The slurm script then launches the job with `srun` by calling `pvbatch`
with `render-particles.py`, which is the name I gave my ParaView script.

Once this script is ready, it can be launched with `sbatch
render-particles.slurm` (where I have named this script
`render-particles.slurm`.

Example Python script: [render-particles.py](render-particles.py)  
Example Slurm launch script: [render-particles.slurm](render-particles.slurm)

[particle visualization]: {{% relref "particle-data.md" %}}
[ParaView Guide]: 
https://docs.paraview.org/en/latest/UsersGuide/introduction.html#getting-started-with-pvpython
[Andes User Guide]: https://docs.olcf.ornl.gov/systems/andes_user_guide.html#running-jobs

---

Next: [Acknowledgements]({{< relref acknowledgements.md >}})
