---
title: "ParaView OLCF Tutorial 2022"
type: "docs"
---

September 15, 2022

# ParaView OLCF Tutorial 2022

Kenneth Moreland, Oak Ridge National Laboratory[^ack]

This tutorial covers using ParaView to process data at the [Oak Ridge
Leadership Computing Facility (OLCF)]. In particular, we focus on running
ParaView on the [Andes HPC system]. In this tutorial, we cover connecting
an interactive session to Andes, performing several visualization
operations on data characteristic of that analyzed at OLCF, and setting up
automated batch processing. Although the particulars of this tutorial focus
on Andes, the procedure is similar for other HPC systems.

[Oak Ridge Leadership Computing Facility (OLCF)]: https://www.olcf.ornl.gov/
[Andes HPC system]: https://www.olcf.ornl.gov/olcf-resources/compute-systems/andes/

<img src="asteroid-impact/thumbnail.png" width="49%" />
<img src="particle-data/thumbnail.png" width="49%" />

## Prerequisites

To get the most out of this tutorial, it will be helpful for participants
to have a basic knowledge of accessing OLCF resources. In particular,
participants should know the basics of [logging into the login node] and
[submitting a job] to Andes (or some equivalent HPC system).

It will also be helpful to have some basic knowledge on the use of
ParaView. This tutorial will explain how to get started for those
completely new to ParaView, but a small amount of familiarity with ParaView
will go a long way in understanding the exercises.

[logging into the login node]: https://docs.olcf.ornl.gov/connecting/index.html
[submitting a job]: https://docs.olcf.ornl.gov/systems/andes_user_guide.html#running-jobs

## Ready, Set, Go

This tutorial is divided into 5 sections, which you can easily navigate
with the left and right sidebars.

1. [Getting Started]({{< relref getting-started.md >}})
2. [Connecting to Andes]({{< relref connecting-to-andes.md >}})
3. [Asteroid Impact]({{< relref asteroid-impact.md >}})
4. [Particle Data]({{< relref particle-data.md >}})
5. [Batch Processing]({{< relref batch-processing.md >}})

---

Next: [Getting Started]({{< relref getting-started.md >}})

---

> Update December 1, 2023: The location of the data files on Andes have
> changed due to changes in the shared drives. The locations have been
> updated in this tutorial.

[^ack]: This research used resources of the Oak Ridge Leadership Computing
    Facility at the Oak Ridge National Laboratory, which is supported by
    the Office of Science of the U.S. Department of Energy under Contract
    No. DE-AC05-00OR22725.
    
    This work was partially supported by the Scientific Discovery through
    Advanced Computing (SciDAC) program in the U.S. Department of Energy under
    grant number DE-FE0031880.
