---
title: "Getting Started"
weight: 1
---

# Getting Started

Before we jump into connecting ParaView to the Andes HPC cluster, let us go
over some preliminaries to get us started.


## Installing the Correct Version of ParaView

When using ParaView interactively on an HPC system, we need ParaView
installed on both the HPC server and on your local computer. We will make
sure that this is installed correctly. ParaView is already installed on
Andes, but you need to make sure that your version of ParaView matches that
on Andes.

We can check what versions of ParaView are currently installed on Andes by
executing the `module avail paraview` command on its login node.

``` sh
andes-login5 0> module avail paraview

------- /sw/andes/spack-envs/base/modules/spack/linux-rhel8-x86_64/Core --------
   paraview/5.8.1-py3-pyapi    paraview/5.9.1-py3-pyapi
   paraview/5.8.1-py3          paraview/5.9.1-py3

-------------------------- /sw/andes/modulefiles/core --------------------------
   paraview/5.9.1-egl       paraview/5.10.0-egl
   paraview/5.9.1-osmesa    paraview/5.10.0-osmesa (D)

  Where:
   D:  Default Module

Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching
any of the "keys".


andes-login5 0>
```

We can see by this output that the most recent version of ParaView on Andes
is ParaView 5.10.0. Note that this is a recent version of ParaView, but not
the most recent version. To connect properly to Andes, you will have to
install and run _the exact same version_ of ParaView on your system. If you
run ParaView 5.10.1 on your system, it will not work with the ParaView
5.10.1 installed on Andes.

If you do not have this version of ParaView on your local desktop/laptop,
install it now. Binaries for all of ParaView's version are available from
the [ParaView download page] maintained by Kitware Inc. Find the
appropriate version, download, and install it.

Note that it is possible to have multiple versions of ParaView installed on
your computer. Just make sure you are running the version that you expect.
The ParaView version is displayed in the GUI's title bar.

[ParaView download page]: https://www.paraview.org/download/


## Getting Started with ParaView

This tutorial assumes a basic knowledge of ParaView with the ability to
open a file, display data, and filter data. There are numerous [ParaView
tutorials] that go into detail about using ParaView.

If this is the first time you have run ParaView, it will start with a
welcome dialog like the following.

![](welcome.png)

This welcome dialog has two links on it: `Getting Started Guide` and
`Example Visualization`. If this is your first time using ParaView, click
the link for `Getting Started Guide` now. This opens a quick, 2-page primer
on using ParaView and contains what you need to follow along with this
tutorial.

[ParaView tutorials]: https://www.paraview.org/tutorials/


## Further Help

Before we continue with the tutorial by [connecting to Andes], we will
point out where to get further help. A good place to find more training and
documentation for ParaView is under the application's help menu.

![](help-menu.png)

In this menu you will find the aforementioned getting started guide as well
as the full ParaView documentation and multiple general tutorials. You will
also find helpful links such as to the [ParaView web site] and the
[community support discourse] where you can ask the ParaView community
questions and search questions already asked.

[connecting to Andes]: {{% relref connecting-to-andes.md %}}
[ParaView web site]: https://www.paraview.org/
[community support discourse]: https://discourse.paraview.org/

---

Next: [Connecting to Andes]({{< relref connecting-to-andes.md >}})
