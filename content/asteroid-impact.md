---
title: "Asteroid Impact"
weight: 20
---

<img src="thumbnail.png" style="float: right;" />

# Asteroid Impact

In this section of the tutorial we will explore data from a shock physics
simulation of an asteroid impact.

<div style="clear: both;"></div>

The data for this tutorial is currently available on OLCF at
`/lustre/orion/world-shared/stf007/msandov1/scivis_datasets/scivis_2018_deep_impact`.
The data originally comes from SciVis 2018 contest:
https://sciviscontest2018.org/. The original simulation was an ensemble
studying the effect a large asteroid impact in the ocean could have and how
it might propagate to land.

## Connect to Andes

One node should be sufficient.

## Open one of the File Sets

1. Click on `Open` file {{< icon Open >}}
2. Navigate to
   `/lustre/orion/world-shared/stf007/msandov1/scivis_datasets/scivis_2018_deep_impact`
3. Navigate to the `yA31` subdirectory (need to open the `yA*` group)
4. Open the `pv_insitu_300x300x300_..vti` time series
5. Turn off the `vtkGhostType` and `vtkValidPointMask` arrays
6. `Apply`

![](open-file.png)
![](file-browser-with-group.png)

## View a Slice of the Data

1. Add the `Slice` filter {{< icon Slice >}}
2. Click the `Z Normal` button
3. Turn `Show Plane` off
4. `Apply`

![](slice.png)

## View the Pressure of the Materials

The asteroid impact sends pressure waves through the air and water. Let's
look at the shock front in the air.

1. Color the slice by `prs`
2. Select `Rescale to Custom Data Range` {{< icon ResetRangeCustom >}}
3. Change the upper range to `2e7`
4. Play {{< icon VcrPlay >}} through timesteps to see how the simulation
   progresses over time.

You can also pause {{< icon VcrPause >}} the playback and browse through
timesteps.

![](slice-color.png)

## View 3D Versions of the Objects in the Scene

The data has volume fraction fields providing the fraction of space
occupied by the water and rock, respectively. We can create contours at the
interface of 50% occupancy to estimate the shape of the objects.

1. Select the reader (`pv_insitu_300x300x300_*.vti`) in the pipeline browser
2. Add the `Contour` filter {{< icon Isosurface >}}
3. Change `Contour By` to `v03`
4. Change the isosurface value to `0.5`
5. `Apply`
6. Color the contour by `Solid Color`
7. Edit the color {{< icon EditColor >}} to a value appropriate for rock

![](contour-rock.png)

Repeat steps 1-7 again except change the `Contour By` value to `v02` in
step 3 for water. Water tends to be transparent, so find the `Opacity`
parameter for the water contour and change it to `0.5`.

![](contour-water.png)

## Volume render the pressure

Now that we have a 3D representation of the objects in space, let us show
that with the air shockwave.

1. Select and turn on the visibility {{< icon Eyeball >}} (if necessary) of
   the reader (`pv_insitu_300x300x300_*.vti`) in the pipeline browser
2. Change the representation to `Volume`
   * ParaView will likely give you a dialog box warning you that things
     might slow down. If so, just click a version of "Yes".
3. Color by `prs`
4. Open the color map editor {{< icon EditColor >}}
5. Modify the opacity to best represent the shockwave

![](volume-render.png)

---

Next: [Particle Data]({{< relref particle-data.md >}})
