---
title: "Acknowledgements"
weight: 100
---

# Acknowledgements

This tutorial was written by

<!--
<table>
<tr>
<td style="border-collapse:collapse; border: none;">
<img
  src="https://www.gravatar.com/avatar/5785d806cffc98ab63daa2d1a43f0c5e?s=150"
  alt="Kenneth Moreland"
  style="border-radius: 25%;" />
</td>
<td style="border-collapse:collapse; border: none;">
Kenneth Moreland<br/>
Oak Ridge National Laboratories<br/>
morelandkd@ornl.gov<br/>
http://kennethmoreland.com<br/>
</td>
</tr>
</table>
-->

<img
  src="https://www.gravatar.com/avatar/5785d806cffc98ab63daa2d1a43f0c5e?s=150"
  alt="Kenneth Moreland"
  style="border-radius: 25%; float: left; margin-right: 10px;" />

Kenneth Moreland  
Oak Ridge National Laboratories  
morelandkd@ornl.gov  
http://kennethmoreland.com  

<div style="clear: both;">&nbsp;</div>
A special thanks to Michael Sandoval for collecting the data and organizing
the event.

<img src="../img/olcf-logo.png" alt="OLCF Logo"
     style="float:right; margin-left: 10px;" />

This research used resources of the Oak Ridge Leadership Computing Facility
at the Oak Ridge National Laboratory, which is supported by the Office of
Science of the U.S. Department of Energy under Contract No.
DE-AC05-00OR22725.

<img src="../img/rapids-logo.jpg" alt="RAPIDS Logo"
     style="float:right; margin-left: 10px;" />

This work was partially supported by the Scientific Discovery through
Advanced Computing (SciDAC) program in the U.S. Department of Energy under
grant number DE-FE0031880.
