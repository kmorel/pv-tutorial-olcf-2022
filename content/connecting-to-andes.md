---
title: "Connecting to Andes"
weight: 10
---

# Connecting to Andes

For this tutorial, we will connect to Andes and operate on some data using
the HPC cluster. The data we will be using is currently residing on the
OLCF shared file system at

```
/lustre/orion/world-shared/stf007/msandov1/scivis_datasets
```

## Opening the Connect Dialog

To connect to any remote ParaView service, start by clicking the `Connect`
button {{% icon Connect %}}. This is on the top toolbar, 6th from the left.
It can also be found in the `File` menu.

When you click this button, it brings up a dialog box to configure and
connect to remote servers.

![](connect-dialog-empty.png)

If this is the first time you have attempted to connected to a remote
server, then the table in this dialog box is empty. If Andes is not listed
in this table, continue to [Adding the Andes Connection Information]. If it
is already there, you can skip to [Connecting].

[Adding the Andes Connection Information]: #adding-the-andes-connection-information
[Connecting]: #connecting

## Adding the Andes Connection Information

Click on the `Fetch Servers` button in the connection dialog box. This will
fetch a list of available pre-configured connections that you can import
into your own configuration.

![](fetch-servers.png)

Scroll to where the ORNL configurations are listed. Select the `ORNL andes`
(and optionally the `ORNL summit`) configurations and click `Import
Selected`. When you do that, you will be brought back to the connection
dialog box. But you will now see your imported configurations listed.

![](connect-dialog.png)

## Connecting

Once you have the `ORNL andes` configuration loaded into the connection
dialog, select the `ORNL andes` configuration and click `Connect`. This
will bring up a second dialog box that allows you to select configuration
parameters.

![](connect-options.png)

The following connection options are available for Andes.

  * `Server host`: The hostname of the login node. Leave this as
    `andes.olcf.ornl.gov`.
  * `Server headless API`: This allows you to select the rendering API used
    by the ParaView server. Switch this to `EGL` so the server will use the
    GPUs for rendering.
  * `Server username`: Change to the username you use to connect to OLCF
    machines.
  * `Server port`: This is a random number used to select a port used to
    connected back to your machine. It is random to avoid multiple people
    using the same port and to make it more difficult for adversaries to
    intercept your connection.
  * `Number of compute nodes`: This is how many nodes will be requested for
    your ParaView server job. For now, we will just connect to `1`.
  * `Total number of MPI tasks`: This is how many ranks will be created in
    the MPI job running the ParaView server. It can be larger than the
    number of compute nodes, in which case multiple MPI processes will run
    on the same nodes. This might be desirable to improve the parallelism
    on the node. For our purposes, we will just be using `1` MPI task.
  * `Number of cores per MPI task`: This will instruct the job launcher to
    allow each MPI rank to use multiple nodes. Since we are only launching
    1 MPI rank per node, let's set this to its maximum of `28`.
  * All jobs scheduled on Andes (or any other OLCF computer) must have a
    project with an allocation to charge. You must belong to the project to
    use it.
  * `Number of minutes to reserve`: All scheduled jobs will have a maximum
    amount of time they can run for when launched. When connecting an
    interactive ParaView session, it is best to keep this time less than 1
    hour or you might be waiting a long time.

Once you have entered all the information, click `OK`. Once you do so,
ParaView will open a terminal in which you must enter your credentials to
connect to the Andes login node (that is, your pin and RSA token code).

![](terminal.png)

After you enter your credentials, you will see the script attempt to launch
the ParaView server. It might take a few minutes to launch, especially if
the system is busy. Once the server successfully runs and connects back to
your client, you will see the GUI restore itself and get ready for usage.

![](connected-gui.png)


## Monitoring Job Time

As mentioned previously, when you launch any job at OLCF, you have to
specify a limited amount of time that the job can run. If you attempt to
use the ParaView server longer than that, the OLCF scheduler will kill your
server and you will lose all progress, which can be frustrating.

To help prevent you from losing your work, ParaView keeps track of how long
the server has been running and how much time you have left. This time is
posted in the `Pipeline Browser` where it identifies what server you are
connected to.

![](connection-time.png)

As you get close to your allotted runtime, ParaView will pop up dialog
boxes reminding you that your job is almost at an end.

![](timeout-warning.png)

If your server job is near completion and you are not yet finished, you can
save your work with `Save State` {{< icon SaveState >}}. This will
save a state file on your local machine. You can then restart the server,
choose `Load State` {{< icon LoadState >}} can continue where you left off.


## Monitoring Memory

If you are using Andes or some other HPC machine to visualize your data, it
is likely because you have a large amount of data. Any time you work with
large data, you run the risk of running out of memory. If any of the nodes
in your job run out of memory, the whole job will crash.

As such, it is helpful to keep track of the memory usage of the system. You
can do this by showing the memory inspector: `View` -> `Memory Inspector`.
The memory inspector will show you the system's overall memory use and the
use of each node in your job. This allows you to monitor the memory used
and adjust if too much memory is being used.

![](memory-inspector.png)

---

Next: [Asteroid Impact]({{< relref asteroid-impact.md >}})
